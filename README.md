# An Introduction to R

These materials accompany the DUPRI "An Introduction to R" workshop. If you have any questions, contact Mark Yacoub at <mark.yacoub@duke.edu>.

* r_intro.Rmd - Rmarkdown HTML version of workshop code
* r_intro.html - HTML report of workshop for reference
* Intro to R.Rproj - RStudio project file
* GSS2022.dta - 2022 GSS data set
* nba_player_stats_2023.csv - NBA player stats for 2023 season
* nba_totals_2023.csv - NBA team total stats for 2023 season
* nba_adv_2023.csv - NBA team advanced stats for 2023 season
* nba_per_game_2017.csv - NBA team per game stats for 2017 season
* nba_per_game_2018.csv - NBA team per game stats for 2018 season
* nba_per_game_2019.csv - NBA team per game stats for 2019 season
* nba_per_game_2020.csv - NBA team per game stats for 2020 season
* nba_per_game_2021.csv - NBA team per game stats for 2021 season
* nba_per_game_2022.csv - NBA team per game stats for 2022 season
* nba_per_game_2023.csv - NBA team per game stats for 2023 season
